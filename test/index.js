const bull = require('bull')

const { redis, InternalError } = require('../src/Settings')

const { queueCreate, queueDelete,
    queueFindOne, queueUpdate,
    queueView } = require('../src/Adapter/index')

async function Create() {

    try {
        const job = queueCreate.add({age,color,name})

        const result = await job.finished()

        console.log(result)

    } catch (error) {
        console.log(error)
    }

}

async function Delete() {

    try {
        const job = queueDelete.add({id})

        const result = await job.finished()

        console.log(result)

    } catch (error) {
        console.log(error)
    }

}

async function FindOne() {

    try {
        const job = queueFindOne.add({name})

        const result = await job.finished()

        console.log(result)

    } catch (error) {
        console.log(error)
    }

}

async function Update() {

    try {
        const job = queueUpdate.add({age,color,id,name})

        const result = await job.finished()

        console.log(result)

    } catch (error) {
        console.log(error)
    }

}

async function View() {

    try {
        const job = queueView.add({})

        const result = await job.finished()

        console.log(result)

    } catch (error) {
        console.log(error)
    }

}

async function main () {
    View()
}