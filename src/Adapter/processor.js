const Services  = require('../Services')
const { InternalError } = require('../Settings')
const { queueView, queueCreate,queueDelete,queueUpdate,queueFindOne } = require('./index')


queueView.process(async function (job, done) {

   try {

      const { } = job.data;

      let { statusCode, data, message } = await Services.View({ });


      done(null, { statusCode, data, message });
   } catch (error) {
      console.log({ step: 'adaptador queueView', error: error.toString() })
      done(null, { statusCode: 500, message: InternalError });
   }


})

queueCreate.process(async function(job, done) {

   try {

      const {age,color,name} = job.data;

      let { statusCode, data, message } = await Services.Create({age,color,name});


      done(null, { statusCode, data, message });
   } catch (error) {
      console.log({ step: 'adaptador queueCreate', error: error.toString() })
      done(null, { statusCode: 500, message: InternalError });
   }


})

queueDelete.process(async function (job, done) {

   try {

      const { id } = job.data;

      let { statusCode, data, message } = await Services.Delete({id});


      done(null, { statusCode, data, message });
   } catch (error) {
      console.log({ step: 'adaptador queueDelete', error: error.toString() })
      done(null, { statusCode: 500, message: InternalError });
   }


})

queueFindOne.process(async function (job, done) {

   try {

      const { name } = job.data;

      let { statusCode, data, message } = await Services.FindOne({name});


      done(null, { statusCode, data, message });
   } catch (error) {
      console.log({ step: 'adaptador queueFindOne', error: error.toString() })
      done(null, { statusCode: 500, message: InternalError });
   }


})

queueUpdate.process(async function (job, done) {

   try {

      const { age,color,id,name} = job.data;

      let { statusCode, data, message } = await Services.Update({age,color,id,name});


      done(null, { statusCode, data, message });
   } catch (error) {
      console.log({ step: 'adaptador queueUpdate', error: error.toString() })
      done(null, { statusCode: 500, message: InternalError });
   }


})


