const { sequelize } = require('../Settings');
const { DataTypes } = require('sequelize');

const Model = sequelize.define('curso', {
    name: { type: DataTypes.STRING },
    age: { type: DataTypes.BIGINT },
    color: { type: DataTypes.STRING },

})

async function SyncDB() {

    try {

        await Model.sync()

    } catch (error) {
        console.log(error)
    }

}

/*const db = [
    {
        id:1,
        info: { name: 'Javier', edad: 25},
        color: 'rojo',
    },
    {
        id:2,
        info: { name: 'Nelson', edad: 28 },
        color: 'rojo',
    },
    {
        id:3,
        info: { name: 'Marcelo', edad: 38},
        color: 'rojo',
    }
]*/

module.exports = { SyncDB, Model }