//const { FindUser, ExistUser } = require('../Controller');
const Controllers = require('../Controller');

async function Create({ age, color, name }) {

    try {

        let { statusCode, data, message } = await Controllers.Create({ age, color, name })
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: 'service Create', error: error.toString() })

        return { statusCode: 500, message: error.toString() };
    }
}

async function Delete({ id}) {

    try {

        let { statusCode, data, message } = await Controllers.Delete({ where: {id}})
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: 'service Delete', error: error.toString() })

        return { statusCode: 500, message: error.toString() };
    }
}

async function Update({ age, color, name,id }) {

    try {

        let { statusCode, data, message } = await Controllers.Update({ age, color, name,id})
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: 'service Update', error: error.toString() })

        return { statusCode: 500, message: error.toString() };
    }
}

async function FindOne({ name }) {

    try {

        let { statusCode, data, message } = await Controllers.FindOne({ where: {name} })
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: 'service FindOne', error: error.toString() })

        return { statusCode: 500, message: error.toString() };
    }
}

async function View({ }) {

    try {

        let { statusCode, data, message } = await Controllers.View({})
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: 'service View', error: error.toString() })

        return { statusCode: 500, message: error.toString() };
    }
}


module.exports = { Create, Delete, Update, FindOne, View }


    /*try {

        const existUser = await ExistUser({ id });
        if (existUser.statusCode !== 200) throw (existUser.message);

        if (!existUser.data) throw ("No existe el usuario");

        const findUser = await FindUser({ id });
        if (findUser.statusCode !== 200) throw (findUser.message);
        if (findUser.data.info.edad > 18) {
            console.log("Eres mayor de edad")
        }
        return { statusCode: 200, data: findUser.data };

        /*const personal = PersonalData({ info });
        
        if (info.edad < 18) throw ('Necesitas ser mayor de edad!!')

            const colors = PreferencesColor({ color });

            return { statusCode: 200, data:{personal,colors}}*/



    /*} catch (error) {
        console.log({ step: 'service Servicio', error: error.toString() })

        return { statusCode: 500, message: error.toString() };

    }
}*/

//module.exports = { Servicio }