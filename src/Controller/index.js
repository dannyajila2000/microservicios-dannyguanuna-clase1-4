//const { db } = require('../Models')
//const { setTimeout } = require('timers/promises');

const { Model } = require('../Models')


async function Create({ name, age, color }) {
    try {

        let instance = await Model.create(
            { name, age, color },
            { fields: ['name', 'age', 'colors'] }
        );

        return { statusCode: 200, data: instance.toJSON() }

    } catch (error) {
        console.log({ step: 'contoller Create' }, error.error.toString())
        return { statusCode: 400, message: error.toString() }
    }


}

async function Delete({ where = {} }) {
    try {

        let instance = await Model.destroy({ where });

        return { statusCode: 200, data: 'OK' }

    } catch (error) {
        console.log({ step: 'contoller Delete' }, error.error.toString())
        return { statusCode: 400, message: error.toString() }
    }

}

async function Update({ name, age, color, id }) {
    try {

        let instance = await Model.update(
            { name, age, color },
            { where: { id } }
        );

        return { statusCode: 200, data: instance[1][0].toJSON() }

    } catch (error) {
        console.log({ step: 'contoller Update' }, error.error.toString())
        return { statusCode: 400, message: error.toString() }
    }

}

async function FindOne({ where = {} }) {
    try {

        let instance = await Model.findOne({ where });

        return { statusCode: 200, data: instance.toJSON() }
    } catch (error) {
        console.log({ step: 'contoller FindOne' }, error.error.toString())
        return { statusCode: 400, message: error.toString() }
    }

}

async function View({ where = {} }) {
    try {

        let instances = await Model.findAll({ where });

        return { statusCode: 200, data: instances }

    } catch (error) {
        console.log({ step: 'contoller View' }, error.error.toString())
        return { statusCode: 200, message: error.toString() }
    }

}



module.exports = { Create, Delete, Update, FindOne, View }










/*async function ExistUser({ id }) {

    try {

        const match = db.some(el => el.id === id);

        return { statusCode: 200, data: match }

    } catch (error) {

        console.log({ step: 'Controlador Controller', error: error.toString() })

        return { statusCode: 500, message: error.toString() };

    }

}

async function FindUser({ id }) {

    try {

        /*if(db.some(el => el.id === id)) {*/
/*const user = db.filter(el => el.id === id)[0]
console.log(user)
await setTimeout(2000);

return { statusCode: 200, data: user };
//}
/*if (info.editor === 'vs') {
    console.log("the editor owner of microsoft")
    console.log("the editor is ", info.editor)
}
if (info.editor === 'android studio') {
    console.log("the editor owner of google")
    console.log("the editor is ", info.editor)

}
if (info.editor === 'atom') {
    console.log("the editor owner of free software")
    console.log("the editor is", info.editor)

}*/
        //return { statusCode: 400, message:"No fue posible encontrar el usuario "}

/*} catch (error) {

    console.log({ step: 'Controlador Controller', error: error.toString() })

    return { statusCode: 500, message: error.toString() };

}

}*/
/*function PreferencesColor({ color }) {

    try {

        var c

    if (color === 'rojo') c = 'azul'

    if (color === 'blanco')  c =  'negro'

    if (color === 'azul') c =  'rojo'

    return { statusCode: 200, data:c}

} catch (error) {
    console.log({step:'Controlador Controller',error:error.toString()})

    return{statusCode: 200, message:error.toString()};
    }

}*/

//module.exports = { FindUser, ExistUser }
